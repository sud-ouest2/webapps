# Modifications apportées à GestSup

La gestion de l'aide à apporter aux utilisateurs passe par l'outil gestsup (cf gestsup.fr).

Celui-ci ne permet pas de renvoyer à l'usager son mot de passe s'il l'a oublié ... ce qui est arrivé dès les 1ers jours d'utilisation chez nous ...

Pour avoir un lien depuis la page d'accueil vous pouvez modifier le fichier login.php pour ajouter à la ligne 338 ceci

    <a href="forgot.php" onclick="show_box(\'forgot-box\'); return false;" class="forgot-password-link"><i class="icon-arrow-left"></i> I forgot my password</a>

