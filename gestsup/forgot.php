<?php
################################################################################
# @Name : ./forgot.php 
# @Desc : send a new password by email
# @call : /index.php
# @Author : Eric Seigne <eric.seigne@sud-ouest.org>
# @Version : 3.1.14
# @Create : 20/09/2017
# @Update : 22/09/2017
################################################################################

//init language
require('localization.php');

//initialize variable
if(!isset($message)) $message = '';
if(!isset($info)) $info = '';

if(!isset($_POST['login'])) $_POST['login'] = '';
if(!isset($_POST['password'])) $_POST['password'] = '';
if(!isset($_POST['password2'])) $_POST['password2'] = '';
if(!isset($_POST['mail'])) $_POST['mail'] = '';
if(!isset($_POST['firstname'])) $_POST['firstname'] = '';
if(!isset($_POST['lastname'])) $_POST['lastname'] = '';
if(!isset($_POST['company'])) $_POST['company'] = '';

if(!isset($user1['company'])) $user1['company'] = '';

//secure HMTL injection
$_POST['login']=strip_tags($_POST['login']);
$_POST['password']=strip_tags($_POST['password']);
$_POST['password2']=strip_tags($_POST['password2']);
$_POST['mail']=strip_tags($_POST['mail']);
$_POST['firstname']=strip_tags($_POST['firstname']);
$_POST['lastname']=strip_tags($_POST['lastname']);

//default values
$defaultprofile=1; //1 is poweruser, 2 is single user 

//connexion script with database parameters
require "connect.php";

//switch SQL MODE to allow empty values with lastest version of MySQL
$db->exec('SET sql_mode = ""');

//load parameters table
$qparameters=$db->query("SELECT * FROM `tparameters`");
$rparameters=$qparameters->fetch();
$qparameters->closeCursor();


if ($rparameters['user_register']==1)
{
    //actions on submit
	if (isset($_POST['submit']))
	{
	    //check inputs
	  if($_POST['mail']) {
	    //check if user id already exist
	    $exist_user = false;
	    $query = $db->query("SELECT * FROM tusers WHERE mail = '".$_POST['mail']."' or login='".$_POST['login']."'");
	    $lelogin = "";
	    while ($row = $query->fetch()) {
	      $lelogin = $row['login'];
	      $exist_user = true;   
	    } 
	    if($exist_user !== true) {
	      $message='<div class="alert alert-danger"><strong><i class="icon-remove"></i> Erreur:</strong> Cette adresse mail n\'existe pas dans notre base de données.<br></div>';
	    }
	    else {
	      //crypt password md5 + salt
	      $salt = substr(md5(uniqid(rand(), true)), 0, 5); // Generate a random key
	      $passclear = substr(md5(uniqid(rand(), true)), 0, 5);
	      $password=md5($salt . md5($passclear)); // store in md5, md5 password + salt
	      //query
	      $mailto=addslashes($_POST['mail']);
	      $db->exec ("UPDATE tusers SET password='$password',salt='$salt' WHERE mail='$mailto'");

	      $headers    = array(
		 'MIME-Version: 1.0',
		 'Content-Type: text/html; charset="UTF-8";',
		 'Content-Transfer-Encoding: 7bit'
		 );
	      
	      $messagemail="<p>Votre nouveau mot de passe a été généré.</p>
<p>Rappel pour vous connecter sur la plate-forme de support:</p>
<ul>
<li>adresse: " . $rparameters['server_url'] . "</li>
<li>identifiant: $lelogin</li>
<li>mot de passe: $passclear</li>
</ul>
<p>Pensez à changer votre mot de passe une fois connecté sur le site...</p>

<pre>
--
Envoyé depuis forgot.php
</pre>";

	      mail($mailto,"[" . $rparameters['company'] . "] - Nouveau mot de passe", $messagemail, implode("\r\n", $headers));
	      //message to display
	      $message='<div class="alert alert-block alert-success"><center><i class="icon-ok green"></i>Votre mot de passe a été modifié et envoyé par email...</center></div>';
	    }
	  }
	}
    
    //display form
    echo'
    <!DOCTYPE html>
    <html lang="fr">
	<head>
		<meta charset="UTF-8" />
		<title>GestSup | Gestion de Support</title>
		<link rel="shortcut icon" type="image/png" href="./images/favicon_ticket.png" />
		<meta name="description" content="gestsup" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<!-- basic styles -->
		<link href="./template/assets/css/bootstrap.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="./template/assets/css/font-awesome.min.css" />
		<!--[if IE 7]>
		  <link rel="stylesheet" href="./template/assets/css/font-awesome-ie7.min.css" />
		<![endif]-->
		<!-- page specific plugin styles -->
		<!-- fonts -->
		<link rel="stylesheet" href="./template/assets/css/ace-fonts.css" />
		<link rel="stylesheet" href="./template/assets/css/jquery-ui-1.10.3.full.min.css" />
		<!-- ace styles -->
		<link rel="stylesheet" href="./template/assets/css/ace.min.css" />
		<link rel="stylesheet" href="./template/assets/css/ace-rtl.min.css" />
		<link rel="stylesheet" href="./template/assets/css/ace-skins.min.css" />
		
		<!--[if lte IE 8]>
		  <link rel="stylesheet" href="./template/assets/css/ace-ie.min.css" />
		<![endif]-->
		<!-- inline styles related to this page -->
		<!-- ace settings handler -->
		<script src="./template/assets/js/ace-extra.min.js"></script>
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
		<script src="./template/assets/js/html5shiv.js"></script>
		<script src="./template/assets/js/respond.min.js"></script>
		<![endif]-->
	</head>
		<body class="login-layout">
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<br />
		<div class="main-container">
			<div class="main-content">
				<div class="row">
					<div class="col-sm-10 col-sm-offset-1">
						<div class="login-container">
							<div class="center">
								<h1>
									<i class="icon-ticket green"></i>
									<span class="white">GestSup</span>
									<span style="font-size: x-small;">'.$rparameters['version'].'</span>
								</h1>
								<h4 class="blue">';if (isset($rparameters['company'])) echo $rparameters['company']; echo' </h4>
								<img style="border-style: none" alt="logo" src="./upload/logo/'; if ($rparameters['logo']=='') echo 'logo.png'; else echo $rparameters['logo'];  echo '" />
							</div>
							<br />
							'.$message.'
							<div class="space-6"></div>
							<div class="position-relative">
							<div id="forgot-box" class="forgot-box visible widget-box no-border">

	<div class="widget-body">
	 <div class="widget-main">
		<h4 class="header red lighter bigger"><i class="icon-key"></i> Retrieve Password</h4>
		
		<div class="space-6"></div>
		
		<p>
			Enter your email and to receive instructions
		</p>
		<form method="post">
			 <fieldset>
				<label class="block clearfix">
					<span class="block input-icon input-icon-right">
						<input type="email" name="mail" class="form-control" placeholder="Email" />
						<i class="icon-envelope"></i>
					</span>
				</label>

				<div class="clearfix">
					<input name="submit" type="submit" class="width-35 pull-right btn btn-sm btn-danger">
						<i class="icon-lightbulb"></i>
						Send Me!
					</button>
				</div>
			  </fieldset>
		</form>
	 </div><!--/widget-main-->
	

	 <div class="toolbar center">
		<a href="./" onclick="show_box(\'login-box\'); return false;" class="back-to-login-link">Back to login <i class="icon-arrow-right"></i></a>
	 </div>
	</div><!--/widget-body-->

</div><!--/forgot-box-->

							</div><!--/position-relative-->
						</div>
					</div><!--/.span-->
				</div><!--/.row-fluid-->
			</div>
			<span style="position: absolute; bottom: 0; right: 0;"><a href="https://gestsup.fr">GestSup.fr</a></span>
		</div><!--/.main-container-->
		<script type="text/JavaScript">
			document.getElementById("login").focus();
		</script>
	';
		// Close database access
		unset($db); 
        echo '
	</body>
</html>';

} else {
    echo '<div class="alert alert-danger"><strong><i class="icon-remove"></i>Erreur:</strong> La fonction d\'enregistrement des utilisateurs est désactivé par votre administrateur.<br></div>';
}


?>
